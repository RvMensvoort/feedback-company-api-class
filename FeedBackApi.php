<?php

/**
 * Api class to communicate with the api for feedbackcompany
 */
class FeedBackApi
{
  const CLIENT_ID = 'xxxxx';

  protected $_accessToken = null;

  /**
   *  Return access token from api.
   */
  protected function getAccessTokenFromApi() {
    $secret = 'xxxx';
    
    $endpoint = 'https://www.feedbackcompany.com/api/v2/oauth2/token';
    $method = 'GET';
    
    $parameters = [
      'client_id' => self::CLIENT_ID,
      'client_secret' => $secret,
      'grant_type' => 'authorization_code',
    ];

    try
    {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $endpoint . '?' . http_build_query($parameters));
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);  
      curl_setopt($ch, CURLOPT_TIMEOUT, 10);           
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
      curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);                                                                                 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($ch);
      curl_close ($ch);
  
      $output = json_decode($result);
      self::_errorHandler($output);

    } catch ( Exception $e) {
      throw new Exception("Error while sending Access token request: " . $e);
    }
    
    return $output;
  }

  /**
   * Retrieve from cache.txt if it has not yet existed for 59 days. 
   * Otherwise retrieve data from api and save to cache.txt file. 
   * 
   * Returns Valid Access token;
   */
  public function getAccessToken() {
    $cacheFile = 'cache.txt';

    /* Access token is 60 days valid */
    $start_date = date('Y-m-d H:m:s');
    $end_date = date('Y-m-d H:m:s', strtotime("+59 days"));
    
    /* If file existed for less then 59 days */
    if (file_exists($cacheFile) && (filemtime($cacheFile) > $end_date)) {
    
      try
      {
        /* Get access token from cache file */
        $fileData = file_get_contents($cacheFile);
        $accessTokenData = json_decode($fileData);
        $file = $accessTokenData->access_token;
      } catch ( Exception $e) {
        throw new Exception("Error retrieving file from content: " . $e);
      }

    } else {
      /* Get access token from api */
      $accessTokenData = self::getAccessTokenFromApi();
      file_put_contents($cacheFile, json_encode($accessTokenData), LOCK_EX);
      $file = $accessTokenData->access_token;
    }
    
    return $file;
  }

  /**
   * Returns array with clients data
   * 
   * page = (An integer greater than 0, default: 1)
   * page_size = (An integer between 1 and 1000, default: 25)
   * sort = (A valid sort value, see table below, default: "fullname")
   * 
   * Allowed sort values:
   *  fullname = Sorts on fullname, ascending.
   *  -fullname = Sorts on fullname, descending.
   */
  public function getClients($page = 1, $pageSize = 25, $sort = 'fullname')
  {
    $endpoint = 'https://www.feedbackcompany.com/api/v2/customers';
    $parameters = [
      "page" => $page,
      "page_size" => $pageSize,
      "sort" => $sort,
    ];

    return self::_request($endpoint, "GET", $parameters);
  }

  /**
   * Returns a singele client data
   * 
   * $clientId = (An integer of valid client id)
   */
  public function getClient($clientId)
  {
    $endpoint = 'https://www.feedbackcompany.com/api/v2/customers/' . $clientId;
    return self::_emptyRequest($endpoint, "GET");
  }

  /**
   * Returns array with images data
   * 
   * page = (An integer greater than 0, default: 1)
   * page_size = (An integer between 1 and 1000, default: 25)
   * sort = (A valid sort value, see table below, default: "-created_at")
   * 
   * Allowed sort values:
   *  created_at = Sorts on created datetime, ascending.
   *  -created_at = Sorts on created datetime, descending.
   */
  public function getImages($page = 1, $pageSize = 25, $sort = '-created_at')
  {
    $endpoint = 'https://www.feedbackcompany.com/api/v2/images';
    $parameters = [
      "page" => $page,
      "page_size" => $pageSize,
      "sort" => $sort,
    ];

    return self::_request($endpoint, "GET", $parameters);
  }

  /**
   * Returns a single image data
   * 
   * $uuid = (An integer of valid uuid)
   */
  public function getImage($uuid)
  {
    $endpoint = 'https://www.feedbackcompany.com/api/v2/images/' . $uuid;
    return self::_emptyRequest($endpoint, "GET");
  }

  /**
   * Returns array with invitations data
   * 
   * page = (An integer greater than 0, default: 1)
   * page_size = (An integer between 1 and 1000, default: 25)
   * sort = (A valid sort value, see table below, default: "-created_at")
   * 
   * Allowed sort values:
   *  created_at = Sorts on created datetime, ascending.
   *  -created_at = Sorts on created datetime, descending.
   */
  public function getInvitations($page = 1, $pageSize = 25, $sort = '-created_at')
  {
    $endpoint = 'https://www.feedbackcompany.com/api/v2/invitations';
    $parameters = [
      "page" => $page,
      "page_size" => $pageSize,
      "sort" => $sort,
    ];

    return self::_request($endpoint, "GET", $parameters);
  }

  /**
   * Returns a single invitation data
   * 
   * $id = (An integer of valid id)
   */
  public function getInvitation($id)
  {
    $endpoint = 'https://www.feedbackcompany.com/api/v2/invitations/' . $id;
    return self::_emptyRequest($endpoint, "GET");
  }

  /**
   * Set acccess token for class
   */
  public function setAccessToken($accessToken) {
    $this->_accessToken = $accessToken;
    return;
  }

  /**
   * Check if api responce is succesfull or return exception.
   */
  protected function _errorHandler($errorResponce)
  {
    if($errorResponce == null) {
      throw new Exception('No valid responce');
    } else if(property_exists($errorResponce, "error") && $errorResponce->error !== false) { 
      throw new Exception($errorResponce->error);
    } else if(property_exists($errorResponce, "success") && $errorResponce->success !== true) { 
      throw new Exception($errorResponce->error);
    }
    return true;
  }

  /**
   * Create request header with access token.
   */
  protected function _createHeader()
  {
    if($this->_accessToken == null) {
      throw new Exception("Set Access token before running a request");
    }

    return  ['Authorization: Bearer '. $this->_accessToken];

  }

  /**
   * Make request with data to api
   * $access_token, $data, $endpoint, $method
   */
  protected function _request($endpoint, $method, $data = [])
  {
    $header = self::_createHeader();
    $output = null;

    /* Api return error if there is no data and request has parameters */
    if($method == "GET" && count($data) > 0) {
      $output = self::_emptyRequest($endpoint);
    }

    if($output == null) {
      try
      {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
     
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);  
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        switch($method) {
          case "POST":
            curl_setopt($ch, CURLOPT_URL, $endpoint);
            $dataString = json_encode($data);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString); 
            curl_setopt($ch, CURLOPT_HTTPHEADER, [ 
                'Content-Type: application/json',
                'Content-Length: ' . strlen($dataString) 
              ]  
            );
            break;
          case "GET":
            curl_setopt($ch, CURLOPT_URL, $endpoint . '?' . http_build_query($data));
            break;
        }
        
        $result = curl_exec($ch);
        curl_close ($ch);
  
        $output = json_decode($result);
        self::_errorHandler($output);
  
      } catch ( Exception $e) {
        throw new Exception('Error while sending ' . $endpoint . ' request: '  . $e);
      }
    }

    return $output;
  }

  /**
   * Make GET request to api without data
   */
  protected function _emptyRequest($endpoint)
  {
    $header = self::_createHeader();

    $result = null;
    try
    {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
   
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');  
      curl_setopt($ch, CURLOPT_TIMEOUT, 30); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_URL, $endpoint);
      
      $result = curl_exec($ch);
      curl_close ($ch);

      $output = json_decode($result);
      self::_errorHandler($output);

    } catch ( Exception $e) {
      throw new Exception('Error while sending ' . $endpoint . ' request: '  . $e);
    }

    return $output;
  }
}
?>
